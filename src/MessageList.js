import React, { useState,useEffectm,Component } from 'react';
import './App.css';
import Row from "react-bootstrap/Row";
import Container from "react-bootstrap/Container";
import Col from "react-bootstrap/Col";
class MessageList extends React.Component {
    state = {
        listItems : [{
            "conversationId": 0,
            "delivered": true,
            "delivery_time": "2019-08-14T09:06:15.608Z",
            "header": "string",
            "id": 0,
            "ip": "string",
            "message": "string",
            "message_id": "string",
            "open_time": "2019-08-14T09:06:15.608Z",
            "opened": true,
            "pattern": 0,
            "sender": "string",
            "subject": "string",
            "timestamp": "2019-08-14T09:06:15.608Z",
            "translation": "string"
        },{
            "conversationId": 0,
            "delivered": true,
            "delivery_time": "2019-08-14T09:06:15.608Z",
            "header": "string",
            "id": 0,
            "ip": "string",
            "message": "string",
            "message_id": "string",
            "open_time": "2019-08-14T09:06:15.608Z",
            "opened": true,
            "pattern": 0,
            "sender": "string",
            "subject": "string",
            "timestamp": "2019-08-14T09:06:15.608Z",
            "translation": "string"
        }]

    }

    render() {

        return (
            <>
                <Container fluid={true} className="itemsContainer" style={{overflowY: 'scroll', height: '100%', padding:0, margin:0}} align="left">
                    {this.state.listItems.map(listItem =>
                        <Row className="message">
                           <Row className="message-row">
                               <Col md={4}>
                                   {listItem.pattern}
                               </Col>
                               <Col md={4}>
                                   {listItem.timestamp}
                               </Col>
                               <Col md={4}>
                                   {listItem.sender}
                               </Col>
                           </Row>
                            <Row className="message-row">
                                {listItem.message}
                            </Row>
                        </Row>)}
                </Container>
                {this.isFetching && 'Fetching more list items...'}
            </>
        );
    }

};

export default MessageList;