import React, { useState,useEffectm,Component } from 'react';
import './App.css';
class ConversationList extends React.Component {
    state = {
        listItems : [{
            "dead": true,
            "email": "string",
            "finished": true,
            "hotelId": 0,
            "id": 0,
            "personId": 0,
            "reference": "string",
            "retries": 0,
            "subject": "string",
            "watson_session_id": "string"
        },{
            "dead": true,
            "email": "string",
            "finished": true,
            "hotelId": 0,
            "id": 0,
            "personId": 0,
            "reference": "string",
            "retries": 0,
            "subject": "string",
            "watson_session_id": "string"
        }]

    }

    render() {

        return (
            <>
                <ul className="itemsContainer" style={{overflowY: 'scroll', height: '100%'}} align="left">
                    {this.state.listItems.map(listItem => <li className="item">
                        <b>With:</b> {listItem.email}
                        <br/>
                        <b>From:</b> {listItem.personId}
                        <br/>
                        <b>Hotel</b> {listItem.hotelId}
                    </li>)}
                </ul>
            </>
        );
    }

};

export default ConversationList;