import React, { useState,useEffectm,Component } from 'react';
import './App.css';
import Row from "react-bootstrap/Row";
import Container from "react-bootstrap/Container";
import Col from "react-bootstrap/Col";
import App from "./App";
import Navbar from "react-bootstrap/Navbar";
import logo from "./logo.png";
import ConversationList from "./ConversationList";
import MessageList from "./MessageList";
class Application extends React.Component {
    GATEWAY = "";
    QUERY = "/api/authenticate";
    constructor(props) {
        super(props);

        this.state = {
            token: null,
            hits: [],
        };
    }
    componentDidMount() {
        var s = "{\"password\": \"admin\",\n" +
            " \"rememberMe\": true,\n" +
            " \"username\": \"admin\"}";
        var token = {
            password:'admin',
            rememberMe: true,
            username: 'admin'
        };
        fetch(this.QUERY, {
            method: 'post',
            headers: new Headers({
                'Content-Type': 'application/json',
                'accept': 'application/json'
            }),
            body: s
        })
            .then(response => console.log(response.json()))
            .catch((error) => {
            console.error(error);
        });


        ;
    }

    render() {

        return (
            <div className="App">
                <Navbar className="lowerBorder">
                    <Navbar.Brand href="#home" style={{margin: 0}} class="float-none">
                        <img
                            alt=""
                            src={logo}
                            width="29%"
                            align="left"
                        />
                    </Navbar.Brand>

                </Navbar>
                <Container fluid={true} className="h-90">
                    <Row className="h-100">
                        <Col md={3} justify-content={true} my={5} no-float={true} className="rightPanelBorder">
                            <ConversationList>
                            </ConversationList>
                        </Col>
                        <Col md={9} no-float={true} style={{padding: 0}}>
                            <MessageList>
                            </MessageList>
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }

}
export default Application;
